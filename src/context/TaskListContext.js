import React, { useState, createContext, useEffect } from 'react';
import uuid from 'uuid';

export const TaskListContext = createContext();

const TaskListContextProvider = (props) => {
  const initialState = JSON.parse(localStorage.getItem('tasks')) || []

  const [tasks, setTasks] = useState(initialState)

  const [editItem, setEditItem] = useState(null);

  useEffect(() => {
    localStorage.setItem('tasks', JSON.stringify(tasks))
  }, [tasks])

  const findItem = id => {
    const item = tasks.find(task => task.id === id)

    setEditItem(item);
  }

  const addTask = (title) => {
    setTasks([...tasks, {title, id: uuid}])
  }

  const editTask = (title, id) => {
    const newTask = tasks.map(task => (task.id ===id ? {title, id} : task))

    setTasks(newTask);
    setEditItem(null)
  }

  const removeTask = id => {
    setTasks(tasks.filter(task => task.id !== id))
  }

  const clearTask = () => {
    setTasks([]);
  }

  return (
    <TaskListContext.Provider value = {{tasks, addTask, removeTask, clearTask, findItem, editTask, editItem}}>
      {props.children}
    </TaskListContext.Provider>
  )
}

export default TaskListContextProvider;