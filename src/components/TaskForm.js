import React, { useContext, useState, useEffect } from 'react';
import { TaskListContext } from '../context/TaskListContext';

const TaskForm = () => {
  const { addTask, clearTask, editTask, editItem } = useContext(TaskListContext);

  const [title, setTitle] = useState('')

  const handleChange = (e) => {
    setTitle(e.target.value);
  }

  const handleSubmit = (e) => {
    e.preventDefault();
    if(!editItem) {
      addTask(title);
      setTitle('');
    } else {
      editTask(title, editItem.id)
    }

  }

  useEffect(() => {
    if (editItem) {
      setTitle(editItem.title)
      console.log(editItem)
    } else {
      setTitle('')
    }
  }, [editItem])

  return (
    <form onSubmit={handleSubmit}
          className="form">
      <input className="task-input"
             onChange={handleChange}
             value={title}
             type='text'
             placeholder='Add Task...'
             required
      />
      <div className="buttons">
        <button className="btn add-task-btn" type="submit">
          {editItem ? 'Edit Task' : 'Add Task'}
        </button>
        <div className="buttons">
        <button className="btn clear-btn" type="submit" onClick={clearTask}>
          Clear task
        </button>
      </div>
      </div>
    </form>
  )
}

export default TaskForm
